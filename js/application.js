$('.popover-ombud').popover({
	selector: "a[rel=popover]",
	trigger: 'hover',
	placement: 'right'
})
 $('.radio.star').rating();

 $.fn.tagcloud.defaults = {
   size: {start: 14, end: 18, unit: 'pt'},
   color: {start: '#cde', end: '#f52'}
 };

 $(function () {
   $('#tagcloud a').tagcloud();
 });

    var chart;
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'graph',
                type: 'bar'
            },
            title: {
                text: null
            },
            xAxis: {
                categories: ['Marketing Automation', 'Lead Management'],
                lineWidth: 3                  
            },
            yAxis: {
                min: 0,
                title: {
                    text: null,
                    align: 'high'
                },
                labels: {
                    overflow: 'justify',
                    enabled: false
                },
                lineWidth: 3,
                gridLineWidth: 0
            },
            tooltip: {
                formatter: function() {
                    return ''+
                        this.y +' percent';
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    },
                    borderRadius: 4
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'This Product',
                data: [57, 87],
                color: 'blue'
            }, {
                name: 'Best Competing Product',
                data: [23, 67],
                color: 'red'
            }]
        });

var chart1;
chart1 = new Highcharts.Chart({
            chart: {
                renderTo: 'reviews',
                type: 'bar'
            },
            title: {
                text: null
            },
            xAxis: {
                categories: ['5 Star', '4 Star', '3 Star', '2 Star', '1 Star'],
                lineWidth: 3                  
            },
            yAxis: {
                min: 0,
                title: {
                    text: null,
                    align: 'high'
                },
                labels: {
                    overflow: 'justify',
                    enabled: false
                },
                lineWidth: 3,
                gridLineWidth: 0
            },
            tooltip: {
                formatter: function() {
                    return ''+
                        this.y +' reviews';
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    },
                    borderRadius: 4,
                    series: {
                        groupPadding: 0
                    }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'This Product',
                data: [4,2,3,0,1],
                color: 'blue'
            }]
        });